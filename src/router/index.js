import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    redirect: '/base',
    component: () => import('@/views/index.vue'),
    children: [
      {
        path: '/base',
        component: () => import('@/components/HelloWorld.vue'),
      },
      {
        path: '/elements',
        component: () => import('@/components/ElfElements.vue'),
      },
      {
        path: '/picker',
        component: () => import('@/components/ModelPicker.vue'),
      },
      {
        path: '/device-light',
        component: () => import('@/components/DeviceLight.vue'),
      },
      {
        path: '/PersonController',
        component: () => import('@/components/PersonController.vue'),
      },
      {
        path: '/TransitionScene',
        component: () => import('@/components/TransitionScene.vue'),
      },
      {
        path: '/WeatherTool',
        component: () => import('@/components/WeatherTool.vue'),
      },
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
